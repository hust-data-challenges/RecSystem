# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: source/protos/keras_layer.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import struct_pb2 as google_dot_protobuf_dot_struct__pb2
from source.protos import layer_pb2 as source_dot_protos_dot_layer__pb2
from source.protos import dnn_pb2 as source_dot_protos_dot_dnn__pb2
from source.protos import fm_pb2 as source_dot_protos_dot_fm__pb2
from source.protos import seq_encoder_pb2 as source_dot_protos_dot_seq__encoder__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='source/protos/keras_layer.proto',
  package='protos',
  syntax='proto2',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x1fsource/protos/keras_layer.proto\x12\x06protos\x1a\x1cgoogle/protobuf/struct.proto\x1a\x19source/protos/layer.proto\x1a\x17source/protos/dnn.proto\x1a\x16source/protos/fm.proto\x1a\x1fsource/protos/seq_encoder.proto\"\x90\x05\n\nKerasLayer\x12\x12\n\nclass_name\x18\x01 \x02(\t\x12,\n\tst_params\x18\x02 \x01(\x0b\x32\x17.google.protobuf.StructH\x00\x12\x37\n\x12periodic_embedding\x18\x03 \x01(\x0b\x32\x19.protos.PeriodicEmbeddingH\x00\x12\x36\n\x12\x61uto_dis_embedding\x18\x04 \x01(\x0b\x32\x18.protos.AutoDisEmbeddingH\x00\x12\x18\n\x02\x66m\x18\x05 \x01(\x0b\x32\n.protos.FMH\x00\x12\'\n\nmask_block\x18\x06 \x01(\x0b\x32\x11.protos.MaskBlockH\x00\x12\"\n\x07masknet\x18\x07 \x01(\x0b\x32\x0f.protos.MaskNetH\x00\x12\x1e\n\x05senet\x18\x08 \x01(\x0b\x32\r.protos.SENetH\x00\x12$\n\x08\x62ilinear\x18\t \x01(\x0b\x32\x10.protos.BilinearH\x00\x12\"\n\x07\x66ibinet\x18\n \x01(\x0b\x32\x0f.protos.FiBiNetH\x00\x12\x1a\n\x03mlp\x18\x0b \x01(\x0b\x32\x0b.protos.MLPH\x00\x12!\n\x03\x64in\x18\x0c \x01(\x0b\x32\x12.protos.DINEncoderH\x00\x12!\n\x03\x62st\x18\r \x01(\x0b\x32\x12.protos.BSTEncoderH\x00\x12!\n\x04mmoe\x18\x0e \x01(\x0b\x32\x11.protos.MMoELayerH\x00\x12*\n\x07seq_aug\x18\x0f \x01(\x0b\x32\x17.protos.SequenceAugmentH\x00\x12\x1e\n\x05ppnet\x18\x10 \x01(\x0b\x32\r.protos.PPNetH\x00\x12#\n\x08text_cnn\x18\x11 \x01(\x0b\x32\x0f.protos.TextCNNH\x00\x42\x08\n\x06params'
  ,
  dependencies=[google_dot_protobuf_dot_struct__pb2.DESCRIPTOR,source_dot_protos_dot_layer__pb2.DESCRIPTOR,source_dot_protos_dot_dnn__pb2.DESCRIPTOR,source_dot_protos_dot_fm__pb2.DESCRIPTOR,source_dot_protos_dot_seq__encoder__pb2.DESCRIPTOR,])




_KERASLAYER = _descriptor.Descriptor(
  name='KerasLayer',
  full_name='protos.KerasLayer',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='class_name', full_name='protos.KerasLayer.class_name', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='st_params', full_name='protos.KerasLayer.st_params', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='periodic_embedding', full_name='protos.KerasLayer.periodic_embedding', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='auto_dis_embedding', full_name='protos.KerasLayer.auto_dis_embedding', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='fm', full_name='protos.KerasLayer.fm', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='mask_block', full_name='protos.KerasLayer.mask_block', index=5,
      number=6, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='masknet', full_name='protos.KerasLayer.masknet', index=6,
      number=7, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='senet', full_name='protos.KerasLayer.senet', index=7,
      number=8, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='bilinear', full_name='protos.KerasLayer.bilinear', index=8,
      number=9, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='fibinet', full_name='protos.KerasLayer.fibinet', index=9,
      number=10, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='mlp', full_name='protos.KerasLayer.mlp', index=10,
      number=11, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='din', full_name='protos.KerasLayer.din', index=11,
      number=12, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='bst', full_name='protos.KerasLayer.bst', index=12,
      number=13, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='mmoe', full_name='protos.KerasLayer.mmoe', index=13,
      number=14, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='seq_aug', full_name='protos.KerasLayer.seq_aug', index=14,
      number=15, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='ppnet', full_name='protos.KerasLayer.ppnet', index=15,
      number=16, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='text_cnn', full_name='protos.KerasLayer.text_cnn', index=16,
      number=17, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='params', full_name='protos.KerasLayer.params',
      index=0, containing_type=None,
      create_key=_descriptor._internal_create_key,
    fields=[]),
  ],
  serialized_start=183,
  serialized_end=839,
)

_KERASLAYER.fields_by_name['st_params'].message_type = google_dot_protobuf_dot_struct__pb2._STRUCT
_KERASLAYER.fields_by_name['periodic_embedding'].message_type = source_dot_protos_dot_layer__pb2._PERIODICEMBEDDING
_KERASLAYER.fields_by_name['auto_dis_embedding'].message_type = source_dot_protos_dot_layer__pb2._AUTODISEMBEDDING
_KERASLAYER.fields_by_name['fm'].message_type = source_dot_protos_dot_fm__pb2._FM
_KERASLAYER.fields_by_name['mask_block'].message_type = source_dot_protos_dot_layer__pb2._MASKBLOCK
_KERASLAYER.fields_by_name['masknet'].message_type = source_dot_protos_dot_layer__pb2._MASKNET
_KERASLAYER.fields_by_name['senet'].message_type = source_dot_protos_dot_layer__pb2._SENET
_KERASLAYER.fields_by_name['bilinear'].message_type = source_dot_protos_dot_layer__pb2._BILINEAR
_KERASLAYER.fields_by_name['fibinet'].message_type = source_dot_protos_dot_layer__pb2._FIBINET
_KERASLAYER.fields_by_name['mlp'].message_type = source_dot_protos_dot_dnn__pb2._MLP
_KERASLAYER.fields_by_name['din'].message_type = source_dot_protos_dot_seq__encoder__pb2._DINENCODER
_KERASLAYER.fields_by_name['bst'].message_type = source_dot_protos_dot_seq__encoder__pb2._BSTENCODER
_KERASLAYER.fields_by_name['mmoe'].message_type = source_dot_protos_dot_layer__pb2._MMOELAYER
_KERASLAYER.fields_by_name['seq_aug'].message_type = source_dot_protos_dot_seq__encoder__pb2._SEQUENCEAUGMENT
_KERASLAYER.fields_by_name['ppnet'].message_type = source_dot_protos_dot_layer__pb2._PPNET
_KERASLAYER.fields_by_name['text_cnn'].message_type = source_dot_protos_dot_layer__pb2._TEXTCNN
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['st_params'])
_KERASLAYER.fields_by_name['st_params'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['periodic_embedding'])
_KERASLAYER.fields_by_name['periodic_embedding'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['auto_dis_embedding'])
_KERASLAYER.fields_by_name['auto_dis_embedding'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['fm'])
_KERASLAYER.fields_by_name['fm'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['mask_block'])
_KERASLAYER.fields_by_name['mask_block'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['masknet'])
_KERASLAYER.fields_by_name['masknet'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['senet'])
_KERASLAYER.fields_by_name['senet'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['bilinear'])
_KERASLAYER.fields_by_name['bilinear'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['fibinet'])
_KERASLAYER.fields_by_name['fibinet'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['mlp'])
_KERASLAYER.fields_by_name['mlp'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['din'])
_KERASLAYER.fields_by_name['din'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['bst'])
_KERASLAYER.fields_by_name['bst'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['mmoe'])
_KERASLAYER.fields_by_name['mmoe'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['seq_aug'])
_KERASLAYER.fields_by_name['seq_aug'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['ppnet'])
_KERASLAYER.fields_by_name['ppnet'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
_KERASLAYER.oneofs_by_name['params'].fields.append(
  _KERASLAYER.fields_by_name['text_cnn'])
_KERASLAYER.fields_by_name['text_cnn'].containing_oneof = _KERASLAYER.oneofs_by_name['params']
DESCRIPTOR.message_types_by_name['KerasLayer'] = _KERASLAYER
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

KerasLayer = _reflection.GeneratedProtocolMessageType('KerasLayer', (_message.Message,), {
  'DESCRIPTOR' : _KERASLAYER,
  '__module__' : 'source.protos.keras_layer_pb2'
  # @@protoc_insertion_point(class_scope:protos.KerasLayer)
  })
_sym_db.RegisterMessage(KerasLayer)


# @@protoc_insertion_point(module_scope)
